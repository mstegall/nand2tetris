//function Sys.init 0
(Sys.init)
@SP
A=M
@0
D=A
@SP
M=D+M
@4000 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@SP // POP
AM=M-1
D=M
@3
M=D
@5000 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@SP // POP
AM=M-1
D=M
@4
M=D
//call Sys.main 0
@RETURN0
D=A
@SP
M=M+1
A=M-1
M=D
@1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@3 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@4 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@5
D=A
@SP
D=M-D
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.main
0;JMP
(RETURN0)
@SP // POP
AM=M-1
D=M
@6
M=D
(LOOP)
@LOOP // goto LOOP
0;JMP
//function Sys.main 5
(Sys.main)
@SP
A=M
M=0
A=A+1
M=0
A=A+1
M=0
A=A+1
M=0
A=A+1
M=0
A=A+1
@5
D=A
@SP
M=D+M
@4001 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@SP // POP
AM=M-1
D=M
@3
M=D
@5001 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@SP // POP
AM=M-1
D=M
@4
M=D
@200 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@LCL // POP
D=M
@1
D=D+A
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D
@40 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@LCL // POP
D=M
@2
D=D+A
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D
@6 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@LCL // POP
D=M
@3
D=D+A
@R13
M=D
@SP
AM=M-1
D=M
@R13
A=M
M=D
@123 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
//call Sys.add12 1
@RETURN1
D=A
@SP
M=M+1
A=M-1
M=D
@1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@3 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@4 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@6
D=A
@SP
D=M-D
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.add12
0;JMP
(RETURN1)
@SP // POP
AM=M-1
D=M
@5
M=D
@LCL // PUSH
D=M
@0
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@LCL // PUSH
D=M
@1
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@LCL // PUSH
D=M
@2
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@LCL // PUSH
D=M
@3
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@LCL // PUSH
D=M
@4
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@SP // ADD
AM=M-1
D=M
A=A-1
M=D+M
@SP // ADD
AM=M-1
D=M
A=A-1
M=D+M
@SP // ADD
AM=M-1
D=M
A=A-1
M=D+M
@SP // ADD
AM=M-1
D=M
A=A-1
M=D+M
@LCL //return
D=M-1
@R13
M=D
@4
A=D-A
D=M
@R14
M=D
@SP // POP ARG 
A=M-1
D=M
@ARG
A=M
M=D
@ARG // SP = ARG + 1
D=M+1
@SP
M=D
@R13 // RESTORE THAT
A=M
D=M
@THAT
M=D
@R13 // RESTORE THIS
AM=M-1
D=M
@THIS
M=D
@R13 // RESTORE ARG
AM=M-1
D=M
@ARG
M=D
@R13 // RESTORE LCL
AM=M-1
D=M
@LCL
M=D
@R14 // GOTO RETURN
A=M
0;JMP
//function Sys.add12 0
(Sys.add12)
@SP
A=M
@0
D=A
@SP
M=D+M
@4002 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@SP // POP
AM=M-1
D=M
@3
M=D
@5002 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@SP // POP
AM=M-1
D=M
@4
M=D
@ARG // PUSH
D=M
@0
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@12 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@SP // ADD
AM=M-1
D=M
A=A-1
M=D+M
@LCL //return
D=M-1
@R13
M=D
@4
A=D-A
D=M
@R14
M=D
@SP // POP ARG 
A=M-1
D=M
@ARG
A=M
M=D
@ARG // SP = ARG + 1
D=M+1
@SP
M=D
@R13 // RESTORE THAT
A=M
D=M
@THAT
M=D
@R13 // RESTORE THIS
AM=M-1
D=M
@THIS
M=D
@R13 // RESTORE ARG
AM=M-1
D=M
@ARG
M=D
@R13 // RESTORE LCL
AM=M-1
D=M
@LCL
M=D
@R14 // GOTO RETURN
A=M
0;JMP