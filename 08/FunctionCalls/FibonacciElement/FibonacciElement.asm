@256
D=A
@SP
M=D
//call Sys.init 0
@RETURN0
D=A
@SP
M=M+1
A=M-1
M=D
@1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@3 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@4 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@5
D=A
@SP
D=M-D
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.init
0;JMP
(RETURN0)
//function Main.fibonacci 0
(Main.fibonacci)
@SP
A=M
@0
D=A
@SP
M=D+M
@ARG // PUSH
D=M
@0
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@SP
AM=M-1
D=M
A=A-1
D=D-M
@l$0
D;JLE
@0
D=!A
@l$1
0;JMP
(l$0)
@0
D=A
(l$1)
@SP
A=M-1
M=D
@SP // if-goto
AM=M-1
D=M
@IF_TRUE
D;JNE
@IF_FALSE // goto IF_FALSE
0;JMP
(IF_TRUE)
@ARG // PUSH
D=M
@0
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@LCL //return
D=M-1
@R13
M=D
@4
A=D-A
D=M
@R14
M=D
@SP // POP ARG 
A=M-1
D=M
@ARG
A=M
M=D
@ARG // SP = ARG + 1
D=M+1
@SP
M=D
@R13 // RESTORE THAT
A=M
D=M
@THAT
M=D
@R13 // RESTORE THIS
AM=M-1
D=M
@THIS
M=D
@R13 // RESTORE ARG
AM=M-1
D=M
@ARG
M=D
@R13 // RESTORE LCL
AM=M-1
D=M
@LCL
M=D
@R14 // GOTO RETURN
A=M
0;JMP
(IF_FALSE)
@ARG // PUSH
D=M
@0
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@SP // SUB
AM=M-1
D=M
A=A-1
M=M-D
//call Main.fibonacci 1
@RETURN1
D=A
@SP
M=M+1
A=M-1
M=D
@1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@3 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@4 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@6
D=A
@SP
D=M-D
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.fibonacci
0;JMP
(RETURN1)
@ARG // PUSH
D=M
@0
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@1 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@SP // SUB
AM=M-1
D=M
A=A-1
M=M-D
//call Main.fibonacci 1
@RETURN2
D=A
@SP
M=M+1
A=M-1
M=D
@1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@3 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@4 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@6
D=A
@SP
D=M-D
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.fibonacci
0;JMP
(RETURN2)
@SP // ADD
AM=M-1
D=M
A=A-1
M=D+M
@LCL //return
D=M-1
@R13
M=D
@4
A=D-A
D=M
@R14
M=D
@SP // POP ARG 
A=M-1
D=M
@ARG
A=M
M=D
@ARG // SP = ARG + 1
D=M+1
@SP
M=D
@R13 // RESTORE THAT
A=M
D=M
@THAT
M=D
@R13 // RESTORE THIS
AM=M-1
D=M
@THIS
M=D
@R13 // RESTORE ARG
AM=M-1
D=M
@ARG
M=D
@R13 // RESTORE LCL
AM=M-1
D=M
@LCL
M=D
@R14 // GOTO RETURN
A=M
0;JMP
//function Sys.init 0
(Sys.init)
@SP
A=M
@0
D=A
@SP
M=D+M
@4 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
//call Main.fibonacci 1
@RETURN3
D=A
@SP
M=M+1
A=M-1
M=D
@1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@3 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@4 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@6
D=A
@SP
D=M-D
@ARG
M=D
@SP
D=M
@LCL
M=D
@Main.fibonacci
0;JMP
(RETURN3)
(WHILE)
@WHILE // goto WHILE
0;JMP