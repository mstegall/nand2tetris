//function SimpleFunction.test 2
(SimpleFunction.test)
@SP
A=M
M=0
A=A+1
M=0
A=A+1
@2
D=A
@SP
M=D+M
@LCL // PUSH
D=M
@0
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@LCL // PUSH
D=M
@1
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@SP // ADD
AM=M-1
D=M
A=A-1
M=D+M
@SP
A=M-1
M=!M
@ARG // PUSH
D=M
@0
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@SP // ADD
AM=M-1
D=M
A=A-1
M=D+M
@ARG // PUSH
D=M
@1
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@SP // SUB
AM=M-1
D=M
A=A-1
M=M-D
@LCL //return
D=M-1
@R13
M=D
@SP // POP ARG 
A=M-1
D=M
@ARG
A=M
M=D
@ARG // SP = ARG + 1
D=M+1
@SP
M=D
@R13 // RESTORE THAT
A=M
D=M
@THAT
M=D
@R13 // RESTORE THIS
AM=M-1
D=M
@THIS
M=D
@R13 // RESTORE ARG
AM=M-1
D=M
@ARG
M=D
@R13 // RESTORE LCL
AM=M-1
D=M
@LCL
M=D
@R13 // GOTO RETURN
A=M-1
A=M
0;JMP