@256
D=A
@SP
M=D
//call Sys.init 0
@RETURN0
D=A
@SP
M=M+1
A=M-1
M=D
@1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@3 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@4 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@5
D=A
@SP
D=M-D
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.init
0;JMP
(RETURN0)
//function Class1.set 0
(Class1.set)
@SP
A=M
@0
D=A
@SP
M=D+M
@ARG // PUSH
D=M
@0
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@SP // POP
AM=M-1
D=M
@Class1.0
M=D
@ARG // PUSH
D=M
@1
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@SP // POP
AM=M-1
D=M
@Class1.1
M=D
@0 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@LCL //return
D=M-1
@R13
M=D
@4
A=D-A
D=M
@R14
M=D
@SP // POP ARG 
A=M-1
D=M
@ARG
A=M
M=D
@ARG // SP = ARG + 1
D=M+1
@SP
M=D
@R13 // RESTORE THAT
A=M
D=M
@THAT
M=D
@R13 // RESTORE THIS
AM=M-1
D=M
@THIS
M=D
@R13 // RESTORE ARG
AM=M-1
D=M
@ARG
M=D
@R13 // RESTORE LCL
AM=M-1
D=M
@LCL
M=D
@R14 // GOTO RETURN
A=M
0;JMP
//function Class1.get 0
(Class1.get)
@SP
A=M
@0
D=A
@SP
M=D+M
@Class1.0 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@Class1.1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@SP // SUB
AM=M-1
D=M
A=A-1
M=M-D
@LCL //return
D=M-1
@R13
M=D
@4
A=D-A
D=M
@R14
M=D
@SP // POP ARG 
A=M-1
D=M
@ARG
A=M
M=D
@ARG // SP = ARG + 1
D=M+1
@SP
M=D
@R13 // RESTORE THAT
A=M
D=M
@THAT
M=D
@R13 // RESTORE THIS
AM=M-1
D=M
@THIS
M=D
@R13 // RESTORE ARG
AM=M-1
D=M
@ARG
M=D
@R13 // RESTORE LCL
AM=M-1
D=M
@LCL
M=D
@R14 // GOTO RETURN
A=M
0;JMP
//function Class2.set 0
(Class2.set)
@SP
A=M
@0
D=A
@SP
M=D+M
@ARG // PUSH
D=M
@0
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@SP // POP
AM=M-1
D=M
@Class2.0
M=D
@ARG // PUSH
D=M
@1
A=D+A
D=M
@SP
M=M+1
A=M-1
M=D
@SP // POP
AM=M-1
D=M
@Class2.1
M=D
@0 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@LCL //return
D=M-1
@R13
M=D
@4
A=D-A
D=M
@R14
M=D
@SP // POP ARG 
A=M-1
D=M
@ARG
A=M
M=D
@ARG // SP = ARG + 1
D=M+1
@SP
M=D
@R13 // RESTORE THAT
A=M
D=M
@THAT
M=D
@R13 // RESTORE THIS
AM=M-1
D=M
@THIS
M=D
@R13 // RESTORE ARG
AM=M-1
D=M
@ARG
M=D
@R13 // RESTORE LCL
AM=M-1
D=M
@LCL
M=D
@R14 // GOTO RETURN
A=M
0;JMP
//function Class2.get 0
(Class2.get)
@SP
A=M
@0
D=A
@SP
M=D+M
@Class2.0 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@Class2.1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@SP // SUB
AM=M-1
D=M
A=A-1
M=M-D
@LCL //return
D=M-1
@R13
M=D
@4
A=D-A
D=M
@R14
M=D
@SP // POP ARG 
A=M-1
D=M
@ARG
A=M
M=D
@ARG // SP = ARG + 1
D=M+1
@SP
M=D
@R13 // RESTORE THAT
A=M
D=M
@THAT
M=D
@R13 // RESTORE THIS
AM=M-1
D=M
@THIS
M=D
@R13 // RESTORE ARG
AM=M-1
D=M
@ARG
M=D
@R13 // RESTORE LCL
AM=M-1
D=M
@LCL
M=D
@R14 // GOTO RETURN
A=M
0;JMP
//function Sys.init 0
(Sys.init)
@SP
A=M
@0
D=A
@SP
M=D+M
@6 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@8 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
//call Class1.set 2
@RETURN1
D=A
@SP
M=M+1
A=M-1
M=D
@1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@3 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@4 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@7
D=A
@SP
D=M-D
@ARG
M=D
@SP
D=M
@LCL
M=D
@Class1.set
0;JMP
(RETURN1)
@SP // POP
AM=M-1
D=M
@5
M=D
@23 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
@15 // PUSH
D=A
@SP
M=M+1
A=M-1
M=D
//call Class2.set 2
@RETURN2
D=A
@SP
M=M+1
A=M-1
M=D
@1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@3 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@4 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@7
D=A
@SP
D=M-D
@ARG
M=D
@SP
D=M
@LCL
M=D
@Class2.set
0;JMP
(RETURN2)
@SP // POP
AM=M-1
D=M
@5
M=D
//call Class1.get 0
@RETURN3
D=A
@SP
M=M+1
A=M-1
M=D
@1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@3 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@4 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@5
D=A
@SP
D=M-D
@ARG
M=D
@SP
D=M
@LCL
M=D
@Class1.get
0;JMP
(RETURN3)
//call Class2.get 0
@RETURN4
D=A
@SP
M=M+1
A=M-1
M=D
@1 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@2 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@3 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@4 // PUSH
D=M
@SP
M=M+1
A=M-1
M=D
@5
D=A
@SP
D=M-D
@ARG
M=D
@SP
D=M
@LCL
M=D
@Class2.get
0;JMP
(RETURN4)
(WHILE)
@WHILE // goto WHILE
0;JMP