// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

// R0 - Previous state

(LOOP)
@ KBD
D = M;
@ BLACK
D; JGT
@ WHITE
0; JMP

(BLACK)
@ SCREEN 
D = A;
@ i
M = D; // initialize i to base screen register
(BLACKLOOP)
@ i 
D = M
A = D
M = -1 // set address i to -1
@ i
M = M + 1 // increment i
D = M
@ 24576 // exclusive end of screen
D = D - A
@LOOP
D ; JEQ
@BLACKLOOP
0 ; JMP

(WHITE)
@ SCREEN 
D = A;
@ i
M = D; // initialize i to base screen register
(WHITELOOP)
@ i 
D = M
A = D
M = 0 // set address i to 0
@ i
M = M + 1 // increment i
D = M
@ 24576 // exclusive end of screen
D = D - A
@ LOOP
D ; JEQ
@ WHITELOOP
0 ; JMP




