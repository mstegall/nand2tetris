// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/03/a/RAM8.hdl

/**
 * Memory of 8 registers, each 16 bit-wide. Out holds the value
 * stored at the memory location specified by address. If load==1, then 
 * the in value is loaded into the memory location specified by address 
 * (the loaded value will be emitted to out from the next time step onward).
 */

CHIP RAM8 {
    IN in[16], load, address[3];
    OUT out[16];

    PARTS:
    // Put your code here:
    Register(in=in, load=LOAD0, out=OUT0);
    Register(in=in, load=LOAD1, out=OUT1);
    Register(in=in, load=LOAD2, out=OUT2);
    Register(in=in, load=LOAD3, out=OUT3);
    Register(in=in, load=LOAD4, out=OUT4);
    Register(in=in, load=LOAD5, out=OUT5);
    Register(in=in, load=LOAD6, out=OUT6);
    Register(in=in, load=LOAD7, out=OUT7);
    Mux8Way16(a=OUT0, b=OUT1, c=OUT2, d=OUT3, e=OUT4, f=OUT5, g=OUT6, h=OUT7, sel=address, out=out);
    DMux8Way(in=load, sel=address, a=LOAD0, b=LOAD1, c=LOAD2, d=LOAD3, e=LOAD4, f=LOAD5, g=LOAD6, h=LOAD7);
}